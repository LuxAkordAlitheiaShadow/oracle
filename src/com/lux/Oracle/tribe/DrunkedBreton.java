package com.lux.Oracle.tribe;

import com.lux.Oracle.oracle.Oracle;

public final class DrunkedBreton extends Tribe
{
    private Oracle drunkenness;

    public DrunkedBreton()
    {
        this.name = "DrunkedBreton";
        this.drunkenness = Oracle.getInstance();
        //Initialisation of probabilities
        this.probability = new Probability();
        this.probability.setRandomTipProbability(0);
        this.probability.setGoodTipProbability(0);
        this.probability.setDisasterProbability(70);
        this.probability.setMiracleProbability(30);

    }


    public void callOracle()
    {
        drunkenness.getListen().listening(this);
    }
}
