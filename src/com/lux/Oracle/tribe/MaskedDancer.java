package com.lux.Oracle.tribe;

import com.lux.Oracle.oracle.Oracle;

public final class MaskedDancer extends Tribe
{
    private Oracle animalsTotem;

    public MaskedDancer()
    {
        name = "MaskedDancer";
        this.animalsTotem = Oracle.getInstance();
        //Initialisation of probabilities
        this.probability = new Probability();
        this.probability.setRandomTipProbability(0);
        this.probability.setGoodTipProbability(20);
        this.probability.setDisasterProbability(0);
        this.probability.setMiracleProbability(0);
    }

    public void callOracle()
    {
        animalsTotem.getListen().listening(this);
    }
}
