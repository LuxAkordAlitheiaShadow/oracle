package com.lux.Oracle.oracle;

/**
 * @Package: com.lux
 * @Author: Titouan 'Lux' Allain
 * @Date: 2/14/22
 * @Time: 5:47 PM
 */
public final class Tip extends Service
{
    // Constructor.
    public Tip()
    {

    }

    // Methods.

    void giveGoodTip()
    {
        System.out.println("It's a good tip !");
    }

    void giveRandomTip()
    {
        System.out.println("It's a random tip !");
    }
}
