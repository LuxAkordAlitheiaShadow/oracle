package com.lux.Oracle.oracle;

import com.lux.Oracle.tribe.Tribe;

public interface ListenerInterface
{
    void listening(Tribe tribe);
}
