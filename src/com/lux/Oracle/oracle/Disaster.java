package com.lux.Oracle.oracle;

import com.lux.Oracle.tribe.Tribe;

/**
 * @Package: com.lux
 * @Author: Titouan 'Lux' Allain
 * @Date: 2/14/22
 * @Time: 5:40 PM
 */
final class Disaster extends Service
{
    // Constructor.
    public Disaster()
    {

    }

    // Methods.

    // Method make or remove disaster according to the current tribe's disaster triggering
    void triggerDisasterEvent( Tribe tribe )
    {
        // Tribe isn't under disaster statement
        if ( !tribe.getUnderDisaster() )
        {
            makeDisaster(tribe);
        }
        // Tribe is under disaster statement
        else
        {
            removeDisaster(tribe);
        }
    }

    void makeDisaster( Tribe tribe )
    {
        System.out.println("It's a disaster");
        tribe.setUnderDisaster(true);
    }

    void removeDisaster(Tribe tribe)
    {
        System.out.println("The disaster has been removed");
        tribe.setUnderDisaster(false);
    }
}
