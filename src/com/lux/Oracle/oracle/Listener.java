package com.lux.Oracle.oracle;

import com.lux.Oracle.tribe.Probability;
import com.lux.Oracle.tribe.Tribe;

import java.util.Random;

/**
 * @Package: com.lux
 * @Author: Titouan 'Lux' Allain
 * @Date: 2/14/22
 * @Time: 5:41 PM
 */
public final class Listener extends Service implements ListenerInterface
{
    // Constructor.
    public Listener()
    {

    }

    // Methods.

    // Method for choosing according to tribe's probabilities what response to this one
    public void listening(Tribe tribe)
    {
        System.out.println("The oracle listening the " + tribe.getName() + " tribe.");
        Probability tribeProbability = tribe.getProbability();
        Random randomizer = new Random();
        int rand = randomizer.nextInt(100); // Rand an integer 0 to 99
        System.out.println("Roll value : " + rand);
        // Oracle response a random tip statement
        if ( rand < tribeProbability.getRandomTipProbability() )
        {
            this.responseRandomTip();
        }
        // Oracle response a good tip statement
        else if ( rand < tribeProbability.getRandomTipProbability() + tribeProbability.getGoodTipProbability() )
        {
            this.responseGoodTip();
        }
        // Oracle response a disaster statement
        else if ( rand < tribeProbability.getDisasterProbability() + tribeProbability.getGoodTipProbability() + tribeProbability.getRandomTipProbability() )
        {
            this.responseDisaster( tribe );
        }
        // Oracle response a miracle statement
        else if ( rand < tribeProbability.getDisasterProbability() + tribeProbability.getGoodTipProbability() + tribeProbability.getRandomTipProbability() + tribeProbability.getMiracleProbability() )
        {
            this.responseMiracle();
        }
        else
        {
            this.responseNothing();
        }
        System.out.println("");
    }

    // Method calling a random tip
    private void responseRandomTip()
    {
        Tip tip = Oracle.getInstance().getTips();
        tip.giveRandomTip();
    }

    // Method calling a good tip
    private void responseGoodTip()
    {
        Tip tip = Oracle.getInstance().getTips();
        tip.giveGoodTip();
    }

    // Method calling a disaster
    private void responseDisaster(Tribe tribe)
    {
        Disaster disaster = Oracle.getInstance().getDisaster();
        disaster.triggerDisasterEvent( tribe );
    }

    // Method calling a miracle
    private void responseMiracle()
    {
        Miracle miracle = Oracle.getInstance().getMiracle();
        miracle.makeMiracle();
    }

    // Method for response nothing
    private void responseNothing()
    {
        System.out.println("The oracle response nothing.");
    }
}
