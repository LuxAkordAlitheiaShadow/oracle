# Oracle

## Introduction

This scholar project is a simulator of response with probabilities for highlight object-oriented programming's concepts, particularly inheritance.

There are tribes calling their Oracle. The Oracle can respond some services according to the tribe and their probabilities.

## Resources

The UML diagrams of this project is in `/Resources/UML` folder in .vpp format (Visual Paradigm), also exported in data.zip and in format `.pnj`.

All screenshot are in `/Resources/Screenshot` folder.

## Running project

This project was developed with IntelliJ with the compiler OpenJDK17.

You just need to run the Main.java with an IDE or compile the project to an executable.

## Features

At the beginning of the simulation, tribes are created with a hard coded probabilities.

The tribes probabilities are :

| Services   | Tribes         |            |               |               |
|------------|----------------|------------|---------------|---------------|
| Services   | Drunked Breton | Black Feet | Masked Dancer | Salted Butter |
| Random Tip | 0              | 0          | 0             | 0.4           |
| Good Tip   | 0              | 0.8        | 0.2           | 0.4           |
| Disaster   | 0.7            | 0          | 0             | 0.1           |
| Miracle    | 0.3            | 0.2        | 0             | 0.1           |

For each cycle, tribes are calling their Oracle and have probabilities to retrieve a response.
The probability is rolled from 0 to 100 and realise a Service according to tribe's probabilities

When a tribe trigger a disaster for a second time, the disaster is removed.

## Screenshot

Here a screenshot of the execution.

![Execution](./Resources/Screenshot/Execution.png)